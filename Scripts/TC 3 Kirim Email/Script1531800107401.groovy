import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('TC 1 Login'), [('url') : 'https://gmail.com', ('username') : 'ellenfi.sinaga@gmail.com'
        , ('password') : 'inipassword'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Button_Compose'))

WebUI.delay(2)

WebUI.setText(findTestObject('TextField_Recipient'), recipient)

WebUI.setText(findTestObject('TextField_Subject'), subject)

WebUI.setText(findTestObject('TextField_MessageBody'), message)

WebUI.waitForElementVisible(findTestObject('Upload_File'), 2)

WebUI.uploadFile(findTestObject('Upload_File'), file)

WebUI.delay(2)

WebUI.click(findTestObject('Button_send'))

CustomKeywords.'get.Screencapture.getEntirePage'('')

