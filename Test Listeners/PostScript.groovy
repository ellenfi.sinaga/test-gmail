import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.apache.commons.lang.SystemUtils

import com.aprisma.katalon.tcp.TestCaseProcessor
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext
import com.kms.katalon.core.logging.KeywordLogger

class PostScript {
	
	@BeforeTestCase
	def sampleBeforeTestCase(TestCaseContext testCaseContext) {
	}

	@AfterTestCase
	def sampleAfterTestCase(TestCaseContext testCaseContext) {
	}
	
	@BeforeTestSuite
	def sampleBeforeTestSuite(TestSuiteContext testSuiteContext) {
	}
	
	@AfterTestSuite
	def sampleAfterTestSuite(TestSuiteContext testSuiteContext) {
		KeywordLogger logging = KeywordLogger.getInstance();
		String homeFolder = System.getProperty("user.dir");
		String reportFolder = logging.getLogFolderPath();
		
		String command;
		if(SystemUtils.IS_OS_WINDOWS){
			command = "cmd /c start java -jar \""+homeFolder+"\\Drivers\\component-katalon-aprisma-0.2.jar\" \""+homeFolder+"\" \""+reportFolder+"\" 3";
		}
		else if(SystemUtils.IS_OS_LINUX){
			command = "nohup java -jar \""+homeFolder+"/Drivers/component-katalon-aprisma-0.2.jar\" \""+homeFolder+"\" \""+reportFolder+"\" 3 2>/dev/null &"
		}
		else{
			command = "nohup java -jar \""+homeFolder+"/Drivers/component-katalon-aprisma-0.2.jar\" \""+homeFolder+"\" \""+reportFolder+"\" 3 2>/dev/null &"
		}
		println(command);
		command.execute();
	}
}